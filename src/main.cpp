#include <fstream>
#include <filesystem>
#include <system_error>

#include "GemtextToHtmlTranslator.hpp"

int main()
{
    using namespace std::filesystem;

    std::error_code error_code;

    path input_path;
    std::cout << "Enter input path:" << std::endl;
    std::cin >> input_path;

    if (!exists(input_path))
    {
        std::cout << "Error: input directory does not exist." << std::endl;
        return 0;
    }

    if (!is_directory(input_path))
    {
        std::cout << "Error: input directory is not a directory." << std::endl;
        return 0;
    }

    path output_path;
    std::cout << "Enter output path:" << std::endl;
    std::cin >> output_path;

    if (!exists(output_path))
    {
        std::cout << "Output directory does not exists. Creating new directory..." << std::endl;
        create_directory(output_path, error_code);
        if (error_code)
        {
            std::cout << "Error: unable to create output directory." << std::endl;
            std::cout << "Reason: " << error_code.message() << '.' << std::endl;
            return 0;
        }
        std::cout << "Successfully created." << std::endl;
    }

    if (!is_directory(output_path))
    {
        std::cout << "Error: output directory is not a directory." << std::endl;
        return 0;
    }

    if (!is_empty(output_path))
    {
        std::cout << "Error: output directory is not empty." << std::endl;
        char proceed_answer;
        std::cout << "Program may overwrite existing files. Proceed? (Y/N)" << std::endl;
        std::cin >> proceed_answer;
        if (proceed_answer != 'Y' && proceed_answer != 'y')
            return 0;
    }
    if (error_code)
    {
        std::cout << "Error: unable to access output directory." << std::endl;
        return 0;
    }

    auto input_directory_iterator = recursive_directory_iterator(input_path, error_code);
    if (error_code)
    {
        std::cout << "Error: unable to access input directory." << std::endl;
        return 0;
    }

    GemtextToHtmlTranslator translator;
    for (auto&& entry : input_directory_iterator)
    {
        path to_path = output_path / relative(entry.path(), input_path);
        if (entry.is_regular_file())
        {
            if (entry.path().extension() == ".gmi")
            {
                to_path.replace_extension(".html");
                std::cout << "Translating " << entry << " to " << to_path <<std::endl;
                std::ifstream gmi_file(entry.path());
                if (!gmi_file.good())
                {
                    std::cout << "Error: unable to open input file " << entry << std::endl;
                    continue;
                }
                std::ofstream html_file(to_path);
                if (!html_file.good())
                {
                    std::cout << "Error: unable to open output file " << to_path << std::endl;
                    continue;
                }
                translator.translateTo(gmi_file, html_file);
                std::cout << "Successfully translated to " << to_path << std::endl;
            }
            else
            {
                std::cout << "Copying " << entry << std::endl;
                copy_file(entry, to_path, copy_options::update_existing, error_code);
                /*
                 * Seems to be a bug of gcc compiler, when fails to update
                 * existing file with reason "File exists".
                 * https://www.reddit.com/r/cpp_questions/comments/swo1xs/copy_file_with_overwirte_existing_throws/
                 * https://github.com/msys2/MSYS2-packages/issues/1937
                 */
                if (error_code)
                {
                    std::cout << "Error: unable to copy " << entry << std::endl;
                    std::cout << "Reason: " << error_code.message() << '.' << std::endl;
                }
                else
                {
                    std::cout << "Successfully copied to " << to_path << std::endl;
                }
            }
        }
        else
        {
            if (!exists(to_path, error_code))
            {
                create_directory(to_path, error_code);
                if (error_code)
                {
                    std::cout << "Error: unable to create " << to_path << std::endl;
                    std::cout << "Reason: " << error_code.message() << '.' << std::endl;
                }
            }
            if (error_code)
            {
                std::cout << "Error: unable to access " << to_path << std::endl;
                std::cout << "Reason: " << error_code.message() << '.' << std::endl;
                continue;
            }
        }
    }
    return 0;
}
