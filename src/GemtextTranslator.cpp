#include "GemtextTranslator.hpp"

#include <iostream>

void GemtextTranslator::translateTo(std::istream& in, std::ostream& out)
{
    std::string link;
    std::string content;
    int header_level;
    bool list_mode = false;
    bool preformatted_mode = false;

    start_document(out);
    while (in.good())
    {
        //If found list marker
        //Checking it before preformatting marker, because it is important
        //to end the list
        if (!preformatted_mode && try_consume_list(in, content))
        {
            if (!list_mode)
            {
                list_mode = true;
                start_list(out);
            }
            write_list_item(out, content);
            continue;
        }
        //If there are no more list items, end the list
        else if (list_mode)
        {
            list_mode = false;
            end_list(out);
        }

        //If found marker of preformatted text (```)
        if (try_consume_pre(in, content))
        {
            if (!preformatted_mode)
            {
                preformatted_mode = true;
                start_preformatted(out);
            }
            else
            {
                preformatted_mode = false;
                end_preformatted(out);
            }
            if (!content.empty())
            {
                std::cerr << "Warning: text after preformatted text marker: "
                          << content << '\n';
            }
        }
        //Writing text in preformatted mode
        else if (preformatted_mode && !in.eof())
        {
            std::getline(in, content);
            write_preformatted(out, content);
        }
        //If found link marker
        else if (try_consume_link(in, link, content))
        {
            write_link(out, link, content);
        }
        //If found header marker
        else if ((header_level = try_consume_header(in, content)) != 0)
        {
            write_header(out, content, header_level);
        }
        //If found blockquotes marker
        else if (try_consume_blockquotes(in, content))
        {
            write_blockquotes(out, content);
        }
        //Writing plain text
        else if (!in.eof())
        {
            std::getline(in, content);
            write_text(out, content);
        }
    }

    if (in.bad())
    {
        std::cerr << "Error: I/O error while reading from stream.\n";
        return;
    }

    if (preformatted_mode)
    {
        std::cerr << "Warning: end of preformatted text is not marked.\n";
        end_preformatted(out);
    }

    if (list_mode)
    {
        std::cerr << "Warning: unexpected error. Possible no new line at the"
                     "end of file\n";
        end_list(out);
    }

    end_document(out);
}


int GemtextTranslator::try_consume_header(std::istream& in, std::string& content)
{
    if (in.peek() != '#')
        return 0;

    int header_level = 0;
    while (in.peek() == '#' && header_level < 3)
    {
        in.get();
        ++header_level;
    }

    if (in.peek() == ' ')
        in.get();
    else
        std::cerr << "Warning: no mandatory space.\n";

    std::getline(in, content);
    return header_level;
}


bool GemtextTranslator::try_consume_pre(std::istream& in, std::string& content)
{
    if (in.peek() == '`')
    {
        in.get();
        if (in.peek() == '`')
        {
            in.get();
            if (in.peek() == '`')
            {
                in.get();
                std::getline(in, content);
                return true;
            }
            in.unget();
        }
        in.unget();
    }
    return false;
}


bool GemtextTranslator::try_consume_link(std::istream& in, std::string& link,
                                         std::string& content)
{
    if (in.peek() == '=')
    {
        in.get();
        if (in.peek() == '>')
        {
            in.get();
            parse_link(in, link, content);
            return true;
        }
        in.unget();
    }
    return false;
}


bool GemtextTranslator::try_consume_list(std::istream& in, std::string& content)
{
    if (in.peek() == '*')
    {
        in.get();
        if (in.peek() == ' ')
            in.get();
        else
            std::cerr << "Warning: no mandatory space.\n";

        std::getline(in, content);
        return true;
    }
    return false;
}


bool GemtextTranslator::try_consume_blockquotes(std::istream& in, std::string& content)
{
    if (in.peek() == '>')
    {
        in.get();
        if (in.peek() == ' ')
            in.get();
        else
            std::cerr << "Warning: no mandatory space.\n";

        std::getline(in, content);
        return true;
    }
    return false;
}


void GemtextTranslator::parse_link(std::istream& in, std::string& link, std::string& content)
{
    std::string line;
    std::getline(in, line);
    auto&& link_start_pos = line.find_first_not_of(' ');
    if (link_start_pos != std::string::npos)
    {
        auto&& link_end_pos = line.find_first_of(' ', link_start_pos+1);
        link = line.substr(link_start_pos, link_end_pos-link_start_pos);
        auto content_start_pos = line.find_first_not_of(' ', link_end_pos+1);
        if (content_start_pos != std::string::npos)
            content = line.substr(content_start_pos, line.size());
        else
            std::cerr << "Warning: no link text.\n";
    }
    else
    {
        std::cerr << "Warning: no link.\n";
    }
}
