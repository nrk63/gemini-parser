#include "GemtextToHtmlTranslator.hpp"

void GemtextToHtmlTranslator::start_document(std::ostream& out)
{
    out << "<!DOCTYPE html>\n<html lang=\"en\">\n<body>" << '\n';
}

void GemtextToHtmlTranslator::end_document(std::ostream& out)
{
    out << "</body>\n</html>" << '\n';
}


void GemtextToHtmlTranslator::write_header(std::ostream& out, std::string_view content,
                                           size_t level)
{
    out << "<h" << level << ">" << content << "</h" << level << ">" << '\n';
}


void GemtextToHtmlTranslator::write_link(std::ostream& out, std::string_view link,
                                         std::string_view content)
{
    out << "<a href=\"" << link << "\">" << content << "</a>" << '\n';
}


void GemtextToHtmlTranslator::write_blockquotes(std::ostream& out, std::string_view content)
{
    out << "<blockquote>" << content << "</blockquote>" << '\n';
}


void GemtextToHtmlTranslator::write_text(std::ostream& out, std::string_view content)
{
    if (!content.empty())
        out << "<p>" << content << "</p>" << '\n';
    else
        out << "<br>" << '\n';
}


void GemtextToHtmlTranslator::write_list_item(std::ostream& out, std::string_view content)
{
    out << "<li>" << content << "</li>" << '\n';
}


void GemtextToHtmlTranslator::start_list(std::ostream& out)
{
    out << "<ul>" << '\n';
}


void GemtextToHtmlTranslator::end_list(std::ostream& out)
{
    out << "</ul>" << '\n';
}


void GemtextToHtmlTranslator::write_preformatted(std::ostream& out, std::string_view content)
{
    out << content << '\n';
}


void GemtextToHtmlTranslator::start_preformatted(std::ostream& out)
{
    out << "<pre>" << '\n';
}


void GemtextToHtmlTranslator::end_preformatted(std::ostream& out)
{
    out << "</pre>" << '\n';
}
