#pragma once

#include "GemtextTranslator.hpp"

class GemtextToHtmlTranslator final : public GemtextTranslator
{
public:
    void start_document(std::ostream& out) override;
    void end_document(std::ostream& out) override;

    void write_header(std::ostream& out, std::string_view content, size_t level) override;
    void write_link(std::ostream& out, std::string_view link, std::string_view content) override;
    void write_blockquotes(std::ostream& out, std::string_view content) override;
    void write_text(std::ostream& out, std::string_view content) override;

    void write_list_item(std::ostream& out, std::string_view content) override;
    void start_list(std::ostream& out) override;
    void end_list(std::ostream& out) override;

    void write_preformatted(std::ostream& out, std::string_view content) override;
    void start_preformatted(std::ostream& out) override;
    void end_preformatted(std::ostream& out) override;
};
