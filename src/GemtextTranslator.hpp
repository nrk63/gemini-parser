#pragma once

#include <iostream>
#include <string>
#include <string_view>

class GemtextTranslator
{
public:
    virtual ~GemtextTranslator() = default;

    /**
     * Translates Gemtext from input stream and writes the result in output stream.
     */
    void translateTo(std::istream& in, std::ostream& out);

protected:
    virtual void start_document(std::ostream& out) = 0;
    virtual void end_document(std::ostream& out) = 0;

    virtual void write_header(std::ostream& out, std::string_view content,
                              size_t level) = 0;
    virtual void write_link(std::ostream& out, std::string_view link,
                            std::string_view content) = 0;
    virtual void write_blockquotes(std::ostream& out, std::string_view content) = 0;
    virtual void write_text(std::ostream& out, std::string_view content) = 0;

    virtual void write_list_item(std::ostream& out, std::string_view content) = 0;
    virtual void start_list(std::ostream& out) = 0;
    virtual void end_list(std::ostream& out) = 0;

    virtual void write_preformatted(std::ostream& out, std::string_view content) = 0;
    virtual void start_preformatted(std::ostream& out) = 0;
    virtual void end_preformatted(std::ostream& out) = 0;

private:
    /**
     * Tries to read header marker. If it is present, reads a line from input
     * stream and places header text in "content", returns header level (1-3).
     * Otherwise, returns 0, no characters consumed. Does not clear state flags.
     */
    static int try_consume_header(std::istream& in, std::string& content);

    /**
     * Tries to read preformatted text marker. If it is present, reads
     * a line from input stream and, if present, places text in "content",
     * returns true. Otherwise, returns false, no characters consumed.
     * Does not clear state flags.
     */
    static bool try_consume_pre(std::istream& in, std::string& content);

    /**
     * Tries to read link marker. If it is present, reads a line from input
     * stream and places link in "link", link text in "content", returns true.
     * Otherwise, returns false, no characters consumed. Does not clear state flags.
     */
    static bool try_consume_link(std::istream& in, std::string& link, std::string& content);

    /**
     * Tries to read list marker. If it is present, reads a line from input
     * stream and places list item text in "content", returns true. Otherwise,
     * returns false, no characters consumed. Does not clear state flags.
     */
    static bool try_consume_list(std::istream& in, std::string& content);

    /**
     * Tries to read blockquotes marker. If it is present, reads a line from
     * input stream and places blockquote text in "content", returns true.
     * Otherwise, returns false, no characters consumed. Does not clear state flags.
     */
    static bool try_consume_blockquotes(std::istream& in, std::string& content);

    /**
     * Splits link and its text and places them in "link" and "content" correspondingly.
     * If any of these is not found, does not change stored value.
     */
    static void parse_link(std::istream& in, std::string& link, std::string& content);
};
